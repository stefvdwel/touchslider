# Range touch support
Adds touch support for HTML range elements.
## Working
It calculates the value using the location of the touch event and the min and max attributes of the target element. The value of the target element is set using JavaScript DOM manipulation.
## Usage
Include rangetouchsupport in your project:
`<script src="rangetouchsupport.js"></script>`