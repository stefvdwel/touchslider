var webpack = require('webpack');

var config = {
    context: __dirname + '/src', // `__dirname` is root of project and `/src` is source
    entry: {
        app: './rangetouchsupport.js',
    },
    output: {
        path: __dirname + '/dist', // `/dist` is the destination
        filename: 'rangetouchsupport.js', // bundle created by webpack it will contain all our app logic. we will link to this .js file from our html page.
    },
    module: {
        rules: [
            {
                test: /\.js$/, // rule for .js files
                exclude: /node_modules/,
                loader: "babel-loader" // apply this loader for js files
            }
        ]
    }
};

module.exports = config;