function rangeTouchHandler(event) {
    let minValue = 0;
    if (event.target.min !== "") {
        minValue = parseInt(event.target.min);
    }
    let maxValue = 100;
    if (event.target.max !== "") {
        maxValue = parseInt(event.target.max);
    }
    let relativeHorizontalTargetOffsetX = event.target.getBoundingClientRect().x;
    let relativeHorizontalTouchPosition = event.touches[0].clientX - relativeHorizontalTargetOffsetX;

    if (relativeHorizontalTouchPosition > 0
        && event.touches[0].clientX < (event.target.clientWidth + relativeHorizontalTargetOffsetX)) {
        let calculatedPercentage = relativeHorizontalTouchPosition / event.target.clientWidth * 100;
        let calculatedValue = map(calculatedPercentage, 0, 100, minValue, maxValue);
        if(event.target.value !== Math.round(calculatedValue)) {
            event.target.value = calculatedValue;
        }
    }
}

export default function initialize() {
    let rangeNodes = document.querySelectorAll('input[type=range]');
    rangeNodes.forEach((rangeNode) => {
        rangeNode.addEventListener('touchstart', rangeTouchHandler, false);
        rangeNode.addEventListener('touchmove', rangeTouchHandler, false);
    });
}

function map(x, inMin, inMax, outMin, outMax) {
    return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}